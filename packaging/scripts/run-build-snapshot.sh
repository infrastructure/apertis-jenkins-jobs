#!/bin/bash
#
# Copyright 2016 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
set -ve

# always prevent any interactive behaviour, like on Jenkins using `docker --tty`
exec < /dev/null

export CCACHE_DIR=$(pwd)/ccache
export V=1  # to undo automake silent output
export TMPDIR=$(pwd)/tmp/

checkout="${PACKAGE}"

build_snapshot_options=

if [ -n "$PACKAGING_ONLY" ]; then
  build_snapshot_options=-p
fi

mkdir -p ${TMPDIR}
cd "${checkout}"

# We don't want these polluting build-snapshot's environment. In particular,
# setting DISTRO to a name that is not one of the ones it knows about
# (debian, suse, slackware etc.) makes apparmor fail to build.
unset COMPONENT
unset CREATE_SNAPSHOT_PROJECT
unset DIFF_ID
unset DISTRO
unset PACKAGE
unset PACKAGING_ONLY
unset PHID
unset PROJECT
unset REVISION
unset URI

python3 -u ../apertis-customizations/development/build-snapshot -s \
  -d ../deb --orig-dir ../tarballs \
  ${build_snapshot_options} \
  localhost \
  2>&1 | tee ../buildlog
EXITCODE="${PIPESTATUS[0]}"

if [ "${EXITCODE}" -ne 0 ]; then
  echo "BUILD FAILED"
  tail -c5000 buildlog > buildlog-tail

  # Output the command that failed, for ease of running it manually.
  echo "To reproduce this build yourself, run:" >&2
  echo "   build-snapshot -s ${build_snapshot_options} localhost" >&2
  echo "in the source directory of your project." >&2
  echo "Only submit a new version of this patch once the build succeeds locally." >&2

  exit ${EXITCODE}
fi
