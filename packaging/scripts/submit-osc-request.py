#!/usr/bin/python3 -u
# -*- coding: utf-8 -*-
#
# Copyright 2017 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import os
import sys
import subprocess

from debian.changelog import Changelog

osc_cmd = ["osc", "-c", os.environ["OSCRC"], 'submitrequest', '--yes', '--cleanup']

# Parent project
project = ":".join([os.environ["PROJECT"],
                    os.environ["DISTRO"],
                    os.environ["COMPONENT"]])
snapshots = ":".join([os.environ["PROJECT"],
                      os.environ["DISTRO"],
                      os.environ["COMPONENT"],
                      "snapshots"])
package = os.environ["PACKAGE"]

os.chdir(package)

tags = subprocess.check_output(['git', 'tag', '--list', '--points-at', 'HEAD'],
    universal_newlines=True,
    stdin=subprocess.DEVNULL).strip().splitlines()

if len(tags) < 1:
    sys.exit(0)

version = Changelog(file = open('debian/changelog', 'r')).version

subprocess.check_call(osc_cmd + [snapshots, package, project,
    '-m', '%s %s' % (package, version)],
    stdin=subprocess.DEVNULL)
