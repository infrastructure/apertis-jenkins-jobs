#!/bin/sh
set -eu

# always prevent any interactive behaviour, like on Jenkins using `docker --tty`
exec < /dev/null

PACKAGE_VERSION=$(cd "${PACKAGE}" && dpkg-parsechangelog -SVersion -n1)
UPSTREAM_VERSION=$(echo "${PACKAGE_VERSION}" | sed -e 's/-.*//')

ln -f tarballs/*_"${UPSTREAM_VERSION}.orig.tar.xz" ./

# generate debian/control if it does not exists
( cd "${PACKAGE}" && test -f debian/control || fakeroot make -f debian/rules debian/control || test -f debian/control )
# build the source package
( cd "${PACKAGE}" && dpkg-buildpackage -uc -us -i -I -S )

test -d deb || mkdir deb
ln -f *_"${PACKAGE_VERSION}_source.changes" *_"${UPSTREAM_VERSION}.orig.tar.xz" *_"${PACKAGE_VERSION}.debian.tar.xz" *_"${PACKAGE_VERSION}.dsc" deb/
