#!/usr/bin/python3 -u
# -*- coding: utf-8 -*-
#
# Copyright 2016 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import glob
import os
import subprocess

from debian import deb822

osc_cmd = ["osc", "-c", os.environ["OSCRC"]]

# Parent project
project = ":".join([os.environ["PROJECT"],
                    os.environ["DISTRO"],
                    os.environ["COMPONENT"]])
snapshots = ":".join([os.environ["PROJECT"],
                      os.environ["DISTRO"],
                      os.environ["COMPONENT"],
                      "snapshots"])
package = os.environ["PACKAGE"]

workspace = os.getcwd()
os.chdir("deb")

changes = glob.glob("*.changes")
assert len(changes) == 1

changes = changes[0]
subprocess.check_call(osc_cmd + ['dput', '--maintained-in-git',
    snapshots, changes],
    stdin=subprocess.DEVNULL)
