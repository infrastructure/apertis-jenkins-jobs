#!/bin/bash
#
# Copyright 2016 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
set -ve

# always prevent any interactive behaviour, like on Jenkins using `docker --tty`
exec < /dev/null

OSC="osc -c ${OSCRC}"
export OSC_CONFIG=${OSCRC}

if [ -z "$COMPONENT" ]; then
  export COMPONENT="$(cat ${PACKAGE}/debian/source/apertis-component || echo target)"
fi

# Parent project
PPROJECT="${PROJECT}:${DISTRO}:${COMPONENT}"

# Snapshot project
SPROJECT="${PPROJECT}:snapshots"

NEW=""
if ${OSC} ls ${PPROJECT} ${PACKAGE}; then
  echo "${PPROJECT}/${PACKAGE} already exists, checking it out"
  ${OSC} co ${PPROJECT} ${PACKAGE} -o old-deb
else
  echo "${PPROJECT}/${PACKAGE} does not already exist"
  # We need to force overwrite (--force) because if ${PACKAGE} already
  # exists in ${SPROJECT} but not in ${PPROJECT}, then
  # "osc ls ${SPROJECT} ${PACKAGE}" will fail, but
  # "osc branch -N ${PPROJECT} ${PACKAGE} ${SPROJECT}" will also fail
  NEW="-N --force"
fi

echo "Checking whether ${PACKAGE} exists in ${SPROJECT}"
if ${OSC} ls ${SPROJECT} ${PACKAGE}; then
  echo "(Yes, it does)"
  ${OSC} co ${SPROJECT} ${PACKAGE} -o deb
elif [ -n "$CREATE_SNAPSHOT_PROJECT" ]; then
  echo "Branching ${PPROJECT}/${PACKAGE} to ${SPROJECT}"
  ${OSC} branch ${NEW} ${PPROJECT} ${PACKAGE} ${SPROJECT}
  ${OSC} co ${SPROJECT} ${PACKAGE} -o deb
fi

mkdir -p deb
mkdir -p tarballs

# Copying the orig tarballs is allowed to fail, if they're not there the build
# should be a full source one (e.g. new package case)
cp -v deb/*orig* tarballs || true
test -d old-deb && cp -v old-deb/*orig* tarballs || true
# Drop current checked out content, rm allowed to fail for the new package case
# where the repo is empty
rm -v deb/[a-zA-Z]* || true
