#!/usr/bin/python3 -u
# -*- coding: utf-8 -*-
#
# Copyright 2017 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import os
import sys
import subprocess

if __name__ == '__main__':
    # Copy before we start modifying
    original_environ = dict(os.environ)

    ade = os.path.join(os.getcwd(), 'apertis-dev-tools', 'tools', 'ade')
    package = original_environ['PACKAGE']
    srcdir = os.path.join(os.getcwd(), package)
    threads = (os.cpu_count() or 2)

    # We don't want these polluting the environment
    for name in (
            'COMPONENT',
            'CREATE_SNAPSHOT_PROJECT',
            'DIFF_ID',
            'DISTRO',
            'PACKAGE',
            'PACKAGING_ONLY',
            'PHID',
            'PROJECT',
            'REVISION',
            'URI',
            ):
        os.environ.pop(name, None)

    subprocess.check_call([
        'git',
        'clean',
        '-fdx',
        ],
        cwd=srcdir,
        stdin=subprocess.DEVNULL)

    if os.path.exists(os.path.join(srcdir, '.gitmodules')):
        subprocess.check_call([
            'git',
            'submodule',
            'init',
            ],
            cwd=srcdir,
            stdin=subprocess.DEVNULL)
        subprocess.check_call([
            'git',
            'submodule',
            'update',
            ],
            cwd=srcdir,
            stdin=subprocess.DEVNULL)

    subprocess.check_call([
        ade,
        'configure',
        '--native',
        '--debug',
        '--',
        '--enable-installed-tests',
        ],
        cwd=srcdir,
        stdin=subprocess.DEVNULL)
    subprocess.check_call([
        ade,
        'build',
        '--verbose',
        '--',
        '-j{}'.format(threads),
        ],
        cwd=srcdir,
        stdin=subprocess.DEVNULL)
    subprocess.check_call([
        ade,
        'export',
        ],
        cwd=srcdir,
        stdin=subprocess.DEVNULL)
