#!/usr/bin/env python3
#
# Usage:
#  OBS_XML=obs.xml
#  JUNIT_XML=junit.xml
#  osc prjresults --xml apertis:18.09:target > $OBS_XML
#  obs-to-junit.py
#  cat $JUNIT_XML

import os
import xml.etree.ElementTree as ET

obs_xml = os.environ['OBS_XML']
junit_xml = os.environ['JUNIT_XML']

obs = ET.parse(obs_xml)
testsuites = ET.Element('testsuites')
for result in obs.getroot():
    testsuite = ET.SubElement(testsuites, 'testsuite')
    for status in result:
        testcase = ET.SubElement(testsuite, 'testcase')
        testcase.attrib['name'] = status.attrib['package']
        child = None
        if status.attrib['code'] in {'unresolvable', 'broken'}:
            child = ET.SubElement(testcase, 'error')
        if status.attrib['code'] == 'failed':
            child = ET.SubElement(testcase, 'failure')
        if status.attrib['code'] == 'excluded':
            child = ET.SubElement(testcase, 'skipped')
        # ignore `disabled` entries: we have a `rebuild` repository that is `disabled` 99% of the times
        # and would just clutter the report
        if child is not None:
            child.attrib['type'] = status.attrib['code']
            details = status.find('details')
            if details is not None:
                child.text = details.text
    # the dot is the hierarchy separator for JUnit, so avoid them in tokens and then join the tokens with dots
    name = '.'.join([result.attrib[i].replace('.', '-') for i in ('project', 'repository', 'arch')])
    testsuite.attrib = {
        'name': name,
        'tests': str(len(testsuite)),
        'errors': str(len(testsuite.findall('./*/error'))),
        'failures': str(len(testsuite.findall('./*/failure'))),
        'skips': str(len(testsuite.findall('./*/skipped'))),
    }

junit = ET.ElementTree(testsuites)
junit.write(junit_xml)
